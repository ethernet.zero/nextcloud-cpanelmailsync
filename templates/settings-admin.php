<?php

script('cpanelmailsync', 'settings');
style('cpanelmailsync', 'settings');

/** @var \OCP\IL10N $l */
/** @var array $_ */

?>

<div id="cpanel-settings" class="section">
    <div id="cpanel-settings-header">
        <h2><?php p($l->t('cPanel Mail Sync'));?></h2>
    </div>
    <div id="cpanel-settings-content">
        <div id="cpanel-indicator" class="msg success inlineblock" style="display: none;"><?php p($l->t('Saved'));?></div>
        <form id="cpanel" action="#" method="post">
            <p>
                <label for="cpanelHost"><?php p($l->t('CPanel host:'));?></label>
                https://<input type="text" id="cpanelHost" name="cpanelHost" value="<?php p($_['cpanelHost']); ?>" data-default="<?php p($_['cpanelHost_default']); ?>" />:2083/
            </p>
            <p>
                <label for="cpanelUser"><?php p($l->t('CPanel user:'));?></label>
                <input type="text" id="cpanelUser" name="cpanelUser" value="<?php p($_['cpanelUser']); ?>" data-default="<?php p($_['cpanelUser_default']); ?>" />
            </p>
            <p><?php p($l->t('The actual value of the API token is not displayed here. If you want to change it, type the new token below.'));?></p>
            <p>
                <label for="cpanelApiToken"><?php p($l->t('CPanel API token:'));?></label>
                <input type="password" id="cpanelApiToken" name="cpanelApiToken" value="********" data-default="<?php p($_['cpanelApiToken_default']); ?>" size="32" />
            </p>
            <p>
                <label for="cpanelDomain"><?php p($l->t('Mail domain:'));?></label>
                <input type="text" id="cpanelDomain" name="cpanelDomain" value="<?php p($_['cpanelDomain']); ?>" data-default="<?php p($_['cpanelDomain_default']); ?>" />
            </p>
            <p>
                <label for="cpanelQuota"><?php p($l->t('Default quota for new accounts:'));?></label>
                <input type="number" id="cpanelQuota" name="cpanelQuota" value="<?php p($_['cpanelQuota']); ?>" data-default="<?php p($_['cpanelQuota_default']); ?>" />&nbsp;MB
            </p>
            <div id="cpanel-settings-header">
                <h3><?php p($l->t('Mail account synchronization events'));?></h3>
            </div>
            <p><?php p($l->t("If you don't want full mail account synchronization, here you can disable some or all of the events to which the app listens."));?></p>
            <p>
		<input type="checkbox" id="cpanelEventAddEnabled" name="cpanelEventAddEnabled" value="true" data-default="true" <?php if ($_['cpanelEventAddEnabled'] == 'true'): ?>checked<?php endif; ?> />
                <label for="cpanelEventAddEnabled">
                    <?php p($l->t('Create new mail accounts when a Nextcloud account is created'));?>
                </label>
            </p>
            <p>
		<input type="checkbox" id="cpanelEventUpdatePasswordEnabled" name="cpanelEventUpdatePasswordEnabled" value="true" data-default="true" <?php if ($_['cpanelEventUpdatePasswordEnabled'] == 'true'): ?>checked<?php endif; ?> />
                <label for="cpanelEventUpdatePasswordEnabled">
                    <?php p($l->t('Set the mail account password when a Nextcloud account password is updated'));?>
                </label>
            </p>
            <p>
		<input type="checkbox" id="cpanelEventDeleteEnabled" name="cpanelEventDeleteEnabled" value="true" data-default="true" <?php if ($_['cpanelEventDeleteEnabled'] == 'true'): ?>checked<?php endif; ?> />
                <label for="cpanelEventDeleteEnabled">
                    <?php p($l->t('Delete the mail account when a Nextcloud account is deleted'));?>
                </label>
            </p>
        </form>
    </div>
</div>
