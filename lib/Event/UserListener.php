<?php

/**
 * This listener responds to user creations, deletions and password updates.
 *
 * @package UserListener
 * @author eth0 <ethernet.zero@gmail.com>
 * @version 0.1
 * @copyright (C) 2020 eth0 <ethernet.zero@gmail.com>
 * @license MPLv2 (https://spdx.org/licenses/MPL-2.0.html)
 */

declare(strict_types=1);

namespace OCA\CPanelMailSync\Event;

use OCP\IConfig;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\User\Events\UserCreatedEvent;
use OCP\User\Events\PasswordUpdatedEvent;
use OCP\User\Events\UserDeletedEvent;
use OCA\CPanelMailSync\Service\CPanelService;

/**
 * @template-implements IEventListener<Event>
 */
class UserListener implements IEventListener {
	/** @var string */
	private $appName;

	/** @var CPanelService */
	private $service;

	/** @var IConfig */
	private $config;

	public function __construct($AppName, CPanelService $service,
		IConfig $config) {
		$this->appName = $AppName;
		$this->service = $service;
		$this->config = $config;
	}

	private function _eventEnabled(string $event): bool {
		return $this->config->getAppValue($this->appName, "cpanelEvent{$event}Enabled") === 'true';
	}

	public function handle(Event $event): void {
		if ($event instanceof UserCreatedEvent && $this->_eventEnabled('Add')) {
			$this->service->createMail($event->getUid(), $event->getPassword());
		} elseif ($event instanceof PasswordUpdatedEvent && $this->_eventEnabled('UpdatePassword')) {
			$this->service->updateMail($event->getUser()->getUID(), $event->getPassword());
		} elseif ($event instanceof UserDeletedEvent && $this->_eventEnabled('Delete')) {
			$this->service->deleteMail($event->getUser()->getUID());
		}
	}
}
