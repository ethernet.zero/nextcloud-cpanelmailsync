<?php

/**
 * Main application class.
 *
 * @package Application
 * @author eth0 <ethernet.zero@gmail.com>
 * @version 0.1
 * @copyright (C) 2020 eth0 <ethernet.zero@gmail.com>
 * @license MPLv2 (https://spdx.org/licenses/MPL-2.0.html)
 */

namespace OCA\CPanelMailSync\AppInfo;

use OCA\CPanelMailSync\Event\UserListener;
use OCP\AppFramework\App;
use OCP\EventDispatcher\IEventDispatcher;
use OCP\User\Events\UserCreatedEvent;
use OCP\User\Events\PasswordUpdatedEvent;
use OCP\User\Events\UserDeletedEvent;

class Application extends App {
	public function __construct(array $urlParams=[]) {
		parent::__construct('cpanelmailsync', $urlParams);

		/* @var IEventDispatcher $eventDispatcher */
		$dispatcher = $this->getContainer()->query(IEventDispatcher::class);

		$dispatcher->addServiceListener(UserCreatedEvent::class, UserListener::class);
		$dispatcher->addServiceListener(PasswordUpdatedEvent::class, UserListener::class);
		$dispatcher->addServiceListener(UserDeletedEvent::class, UserListener::class);
	}
}
