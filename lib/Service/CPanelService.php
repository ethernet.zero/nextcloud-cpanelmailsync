<?php

/**
 * Provides actions to operate on cPanel mail accounts.
 *
 * @package CPanelService
 * @author eth0 <ethernet.zero@gmail.com>
 * @version 0.1
 * @copyright (C) 2020 eth0 <ethernet.zero@gmail.com>
 * @license MPLv2 (https://spdx.org/licenses/MPL-2.0.html)
 */

declare(strict_types=1);

namespace OCA\CPanelMailSync\Service;

use \OCP\IConfig;
use \OCP\ILogger;
use \OCP\Http\Client\IClientService;
use \OCP\Http\Client\IClient;

class CPanelService {
	/** @var string */
	private $appName;

	/** @var \OCP\IConfig */
	private $config;

	/** @var \OCP\ILogger */
	private $logger;

	/** @var array */
	private $headers;

	/** @var array */
	private $httpContext;

	/** @var string */
	private $cpanelUrlTemplate = 'https://%s:2083/execute/Email/%s_pop';

	/** @var \OCP\Http\Client\IClient */
	private $http;

	public function __construct($AppName, IConfig $config, ILogger $logger,
		IClientService $httpService) {

		$this->appName = $AppName;
		$this->config = $config;
		$this->logger = $logger;
		$this->http = $httpService->newClient();

		/**
		 * Authorization: cpanel {$userName}:{$apiToken}
		 */
		$cpanelUser = $this->_config('cpanelUser');
		$apiToken = $this->_config('cpanelApiToken');
		$this->headers = [
			'Content-Type' => 'application/x-www-form-urlencoded',
			'Authorization' => "cpanel $cpanelUser:$apiToken",
		];
	}

	private function _config(string $key): string {
		return $this->config->getAppValue($this->appName, $key);
	}

	private function _url(string $action): string {
		return sprintf(
			$this->cpanelUrlTemplate,
			$this->_config('cpanelHost'),
			$action
		);
	}

	private function cpanelEmail(string $action, array $payload): bool {
		try {
			$response = $this->http->post(
				$this->_url($action),
				[
					'headers' => $this->headers,
					'debug' => false,
					'body' => $payload,
				]
			);
			$body = $response->getBody();

			$json = json_decode($body, true);

			if (!is_null($json['messages'])) {
				foreach ($json['messages'] as $message) {
					if (!empty($message)) {
						$this->logger->info($message);
					}
				}
			}

			if (!is_null($json['warnings'])) {
				foreach ($json['warnings'] as $message) {
					if (!empty($message)) {
						$this->logger->warning($message);
					}
				}
			}

			if (!is_null($json['errors'])) {
				foreach ($json['errors'] as $message) {
					if (!empty($message)) {
						$this->logger->error($message);
					}
				}
			}

			return $json['status'] == 1;
		}
		catch (\Exception $e) {
			$url_host = $this->_config('cpanelHost');
			$this->logger->error("The HTTP request to " .
				"$url_host:2083 failed.");
			return false;
		}
	}

	/**
	 * New:
	 * https://{$domain}:2083/execute/Email/add_pop
	 * email={$userName}
	 * domain={$domain}
	 * password={$password}
	 * quota={$quota}
	 * send_welcome_email={$sendWelcomeEmail}
	 */
	public function createMail($userName, $password): void {
		$this->cpanelEmail('add', [
			'email' => $userName,
			'domain' => $this->_config('cpanelDomain'),
			'password' => $password,
			'quota' => $this->_config('cpanelQuota'),
			'send_welcome_email' => 0,
		]);
	}

	/**
	 * Update password:
	 * https://{$domain}:2083/execute/Email/passwd_pop
	 * email={$userName}
	 * domain={$domain}
	 * password={$password}
	 */
	public function updateMail($userName, $password): void {
		$this->cpanelEmail('passwd', [
			'email' => $userName,
			'domain' => $this->_config('cpanelDomain'),
			'password' => $password,
		]);
	}

	/**
	 * Delete:
	 * https://{$domain}:2083/execute/Email/delete_pop
	 * email={$userName}
	 * domain={$domain}
	 */
	public function deleteMail($userName): void {
		$this->cpanelEmail('delete', [
			'email' => $userName,
			'domain' => $this->_config('cpanelDomain'),
		]);
	}
}
